#Diablo 2 Mod

##Setup

Clone this repo into your Diablo II installation folder.
It should be in its own folder directly next to the `Diablo II.exe` file in order to use the batch files.

You should be able to run the game using the `BetterModFullscreen.bat` file for fullscreen mode, or `BetterModWindowed.bat` for windowed mode.

##Change List

* Enabled ladder-only rune words
* Enabled ladder-only cube recipies
* Enabled ladder-only unique items
* Removed extra exp scaling at high levels
* Removed Vampire effect from Trang Oul's Avatar set
* Mercenaries:
    * Act 2 mercenaries now have their Nightmare auras on all difficulties
    * All mercenaries now have their normal mode stat scaling in all difficulties, so hiring a mercenary in Nightmare and Hell is no longer worse than hiring one in Normal
    * Uncap mercenary Holy Freeze and Thorns auras like other Act 2 mercenary auras
